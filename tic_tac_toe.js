let x = 1, o = 0, cellSelected = 0, xWin = 0, oWin = 0, clicksNo = 0;

function play(id) {
  if (document.getElementById(id).innerHTML === "") {
    if ((x === 1) && (o === 0)) {
      document.getElementById(id).innerHTML = "X";
      ++clicksNo;
      x = 0;
      o = 1;
    } else if ((x === 0) && (o === 1)) {
      document.getElementById(id).innerHTML = "O";
      ++clicksNo
      x = 1;
      o = 0;
    } 
  } else if (document.getElementById(id).innerHTML != "") {
    cellSelected = 1;
  }
  if (ifWon("X") === true) {
    xWin = 1;
  } else if (ifWon("O") === true) {
    oWin = 1;
  }
  showStatus();
}

function showStatus() {
  if ((x === 1) && (o === 0) && (cellSelected === 0) && (xWin === 0) && (oWin === 0) && (clicksNo < 9)) {
    document.getElementById("status").innerHTML = "It is the turn of X's player!";
  } else if ((x === 0) && (o === 1) && (cellSelected === 0) && (xWin === 0) && (oWin === 0) && (clicksNo < 9)) {
    document.getElementById("status").innerHTML = "It is the turn of O's player!";
  } else if ((cellSelected === 1) && (xWin === 0) && (oWin === 0) && (clicksNo < 9)){
    document.getElementById("status").innerHTML = "This cell was already selected. Please, choose another one!";
    cellSelected = 0;
  } else if (xWin === 1) {
    document.getElementById("status").innerHTML = "Congrats! X's player has won! Play a new round by refreshing the page";
  } else if (oWin === 1) {
    document.getElementById("status").innerHTML = "Congrats! O's player has won! Play a new round by refreshing the page";
  } else if ((oWin === 0) && (xWin === 0) && (clicksNo === 9)) {
    document.getElementById("status").innerHTML = "It's a tie! Play a new round by refreshing the page";
  }
}

function ifWon(c) {
  if ((document.getElementById("leftTop").innerHTML === c) && (document.getElementById("leftMiddle").innerHTML === c) && (document.getElementById("leftDown").innerHTML === c)) {
    return true;
  } else if ((document.getElementById("middleTop").innerHTML === c) && (document.getElementById("middleMiddle").innerHTML === c) && (document.getElementById("middleDown").innerHTML === c)) {
    return true;
  } else if ((document.getElementById("rightTop").innerHTML === c) && (document.getElementById("rightMiddle").innerHTML === c) && (document.getElementById("rightDown").innerHTML === c)) {
    return true;
  } else if ((document.getElementById("leftTop").innerHTML === c) && (document.getElementById("middleTop").innerHTML === c) && (document.getElementById("rightTop").innerHTML === c)) {
    return true;
  } else if ((document.getElementById("leftMiddle").innerHTML === c) && (document.getElementById("middleMiddle").innerHTML === c) && (document.getElementById("rightMiddle").innerHTML === c)) {
    return true;
  } else if ((document.getElementById("leftDown").innerHTML === c) && (document.getElementById("middleDown").innerHTML === c) && (document.getElementById("rightDown").innerHTML === c)) {
    return true;
  } else if ((document.getElementById("leftDown").innerHTML === c) && (document.getElementById("middleMiddle").innerHTML === c) && (document.getElementById("rightTop").innerHTML === c)) {
    return true;
  } else if ((document.getElementById("leftTop").innerHTML === c) && (document.getElementById("middleMiddle").innerHTML === c) && (document.getElementById("rightDown").innerHTML === c)) {
    return true;
  }
  return false;
}

showStatus();
